﻿using System;

namespace Models
{
    public class Coin
    {
        public string Name;
        public string Symbol;
        public int Rank;
        public decimal Roi;
        public int Size;
    }
}
