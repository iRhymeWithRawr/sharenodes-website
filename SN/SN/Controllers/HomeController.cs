using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SN.Models;

namespace SN.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            throw new NotImplementedException();
        }

        public IActionResult Masternodes()
        {
            ViewData["Message"] = "Your contact page.";

            throw new NotImplementedException();
        }

        public IActionResult FAQ()
        {
            ViewData["Message"] = "Your contact page.";

            throw new NotImplementedException();
        }

       public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
