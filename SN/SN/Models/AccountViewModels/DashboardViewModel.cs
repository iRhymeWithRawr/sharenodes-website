﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models;

namespace SN.Models.AccountViewModels
{
    public class DashboardViewModel
    {
        public UserModel User { get; set; }
        public string Email { get; set; }

    }
}
